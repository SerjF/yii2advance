<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?php  Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn' ,
                'template' => '{view} {update} {delete} {check}',
                'buttons' =>[
                    'check' => function ( $url ,$model , $key){
                    return Html::a('<i class="fas fa-ban"></i>' , $url);
                    }
                ],
            
            'visibleButtons'=>[
                'check'=> function ($model , $key , $index){
                    return ($model->status_id ==0)?false:true;
                }
            ]
           
        ],


            'id',
            'title',
           // 'text:ntext',
            ['attribute'=>'url' , 'format'=>'raw'],
            ['attribute'=>'status_id', 'filter'=>\common\models\Blog::getStatusList(), 'value'=>'StatusName'],
            'sort',

           
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
