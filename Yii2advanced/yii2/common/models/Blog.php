<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Blog".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $url
 * @property int $status_id
 * @property int $sort
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'text', 'url'], 'required'],
            [['id', 'status_id', 'sort'], 'integer'],
            [['text'], 'string'],
            [['url'], 'unique'],
            [['sort'], 'integer' , 'max' =>99, 'min'=>1],
            [['title'], 'string', 'max' => 150],
            [['url'], 'string', 'max' => 200],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Рубрика',
            'text' => 'Текс',
            'url' => 'Адресс',
            'status_id' => 'Статус',
            'sort' => 'Сортировка',
        ];
    }
    public static function getStatusList(){
        return ['off' , 'on'];
    }
    public function getStatusName(){
        $list = self::getStatusList();
        return $list[$this->status_id];
    }
}
